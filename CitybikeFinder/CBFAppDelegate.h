//
//  CBFAppDelegate.h
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import <UIKit/UIKit.h>

@interface CBFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
