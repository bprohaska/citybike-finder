//
//  CBFCityBikeStation.h
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CBFCityBikeStation : NSObject <MKAnnotation>

@property (assign, nonatomic) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString* title;
@property (copy, nonatomic) NSString* subtitle;

-(id)initWithLatitute:(NSNumber*)latitute longitute:(NSNumber*)longitute;
-(id)initWithJSONDictionary:(NSDictionary*)stationInfo;

@end
