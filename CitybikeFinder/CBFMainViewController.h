//
//  CBFMainViewController.h
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import "CBFFlipsideViewController.h"
#import "CBFCityBikeStationProvider.h"
#import <MapKit/MapKit.h>

@interface CBFMainViewController : UIViewController     <CBFFlipsideViewControllerDelegate,
                                                        MKMapViewDelegate,
                                                        CityBikeStationProviderDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CBFCityBikeStationProvider* cityBikeStationProvider;

@end
