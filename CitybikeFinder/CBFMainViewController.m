//
//  CBFMainViewController.m
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import "CBFMainViewController.h"
#import "CBFCityBikeStation.h"

@interface CBFMainViewController ()

@end

@implementation CBFMainViewController {
    
    MKRoute* routeDetails;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    //Setup our data provider...
    self.cityBikeStationProvider            = [[CBFCityBikeStationProvider alloc] init];
    self.cityBikeStationProvider.delegate   = self;
    
    //fetch and wait. The function will return instantly and we will get called back, when data is available.
    [self.cityBikeStationProvider fetchCityBikeStations];
    
    //We want map updates...
    self.mapView.delegate = self;
    
    //Show us...
    [self.mapView setShowsUserLocation:YES];

}

//this function is being called when the map updates the user location. We swipe in with a nice zoom effect to the user.
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.userLocation.coordinate;
    mapRegion.span = MKCoordinateSpanMake(0.1, 0.1);
    
    [self.mapView setRegion:mapRegion animated:YES];

}

//This is the callback of the citybike station provdider. We will just insert all POIs onto the map.
- (void)provider:(CBFCityBikeStationProvider *)provider hasCityBikeStationsAvailable:(NSArray *)stations {
    
    [self.mapView addAnnotations:stations];
    
}

- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {

    //nothing - that is the current user location (the blue dot)
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    
    //Show the funny city bike POI
    MKAnnotationView* theView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
    
    theView.image           =   [UIImage imageNamed:@"citybike"];
    theView.canShowCallout  =   YES;
    theView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return theView;
    
}

//This is the navigation part. when the user clicks the "info" button on the callout accessory view, we will guide him there...
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    //Directions to here...
    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    
    CBFCityBikeStation* destination = (CBFCityBikeStation*)view.annotation;
    
    //Where to??
    MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:destination.coordinate addressDictionary:nil];
    
    //From where we are...
    [directionsRequest setSource:[MKMapItem mapItemForCurrentLocation]];
    
    //To where we want to go...
    [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:placemark]];
    
    //By foot.. :)
    directionsRequest.transportType = MKDirectionsTransportTypeWalking;
    
    //Get some directions...
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
    
        if (error) {
            NSLog(@"Error %@", error.description);
            
        } else {
            
            //Remove the current polyline, if there is one
            if(routeDetails.polyline != nil) {
                [self.mapView removeOverlay:routeDetails.polyline];
            }
            
            //Just draw the line between source and destination...
            routeDetails = response.routes.lastObject;
            
            [self.mapView addOverlay:routeDetails.polyline];
          
        }
    }];
    
}

//This delegate asks for a renderer to render the polyline. Lets make it a red line and 5 points wide.
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {

    MKPolylineRenderer* routeLineRenderer   = [[MKPolylineRenderer alloc] initWithPolyline:routeDetails.polyline];
    routeLineRenderer.strokeColor           = [UIColor redColor];
    routeLineRenderer.lineWidth             = 5;
    return routeLineRenderer;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View

- (void)flipsideViewControllerDidFinish:(CBFFlipsideViewController *)controller
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showAlternate"]) {
        [[segue destinationViewController] setDelegate:self];
    }
}

@end
