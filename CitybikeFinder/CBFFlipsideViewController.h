//
//  CBFFlipsideViewController.h
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import <UIKit/UIKit.h>

@class CBFFlipsideViewController;

@protocol CBFFlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(CBFFlipsideViewController *)controller;
@end

@interface CBFFlipsideViewController : UIViewController

@property (weak, nonatomic) id <CBFFlipsideViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end
