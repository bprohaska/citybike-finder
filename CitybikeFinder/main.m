//
//  main.m
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import <UIKit/UIKit.h>

#import "CBFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CBFAppDelegate class]));
    }
}
