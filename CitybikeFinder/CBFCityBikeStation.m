//
//  CBFCityBikeStation.m
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import "CBFCityBikeStation.h"

@implementation CBFCityBikeStation

-(id)initWithJSONDictionary:(NSDictionary*)stationInfo {

    self = [super init];
    
    if(self) {
    
        NSNumber* latitute = [[[stationInfo objectForKey:@"geometry"] objectForKey:@"coordinates"] objectAtIndex:1];
        NSNumber* longitute = [[[stationInfo objectForKey:@"geometry"] objectForKey:@"coordinates"] objectAtIndex:0];
        
        self.coordinate = CLLocationCoordinate2DMake([latitute doubleValue], [longitute doubleValue]);
        
        //see if the coordinate is valid
        if(CLLocationCoordinate2DIsValid(self.coordinate) == NO) {
            
            return nil;
            
        }

        NSString* district = [NSString stringWithFormat:@"%li. Bezirk", [[[stationInfo objectForKey:@"properties"] objectForKey:@"BEZIRK"] integerValue]];
        NSString* stationName = [NSString stringWithFormat:@"%@", [[stationInfo objectForKey:@"properties"] objectForKey:@"STATION"]];
        
        self.title      = stationName;
        self.subtitle   = district;
        
    }
    
    return self;
    
}

-(id)initWithLatitute:(NSNumber*)latitute longitute:(NSNumber*)longitute {
    
    self = [super init];
    
    if(self) {
    
        self.coordinate = CLLocationCoordinate2DMake([latitute doubleValue], [longitute doubleValue]);
        
        //see if the coordinate is valid
        if(CLLocationCoordinate2DIsValid(self.coordinate) == NO) {
            
            return nil;
            
        }
    }
    
    return self;
}

@end
