//
//  CBFCityBikeStationProvider.h
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

//This is the json URL String
#define kCityBikeJSONURLString @"http://data.wien.gv.at/daten/wfs?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:CITYBIKEOGD&srsName=EPSG:4326&outputFormat=json"
#import <Foundation/Foundation.h>

@class CBFCityBikeStationProvider;

//Our delegate protocol for the station provider. (implemented by the main view controller)
@protocol CityBikeStationProviderDelegate <NSObject>

-(void)provider:(CBFCityBikeStationProvider*)provider hasCityBikeStationsAvailable:(NSArray*)stations;

@end

@interface CBFCityBikeStationProvider : NSObject

@property (strong, nonatomic) NSArray* cityBikeStations;
@property (weak, nonatomic) id<CityBikeStationProviderDelegate> delegate;

//Async fetch
-(void)fetchCityBikeStations;

@end
