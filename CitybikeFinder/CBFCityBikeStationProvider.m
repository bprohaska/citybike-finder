//
//  CBFCityBikeStationProvider.m
//  CitybikeFinder
//
//  Created by Boris Prohaska on 07/05/14.
//
//

#import "CBFCityBikeStationProvider.h"
#import "CBFCityBikeStation.h"

@implementation CBFCityBikeStationProvider

-(void)fetchCityBikeStations {
    
    //Do background work and call the UI when finished.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
        
        NSURL *url = [NSURL URLWithString:kCityBikeJSONURLString];
        NSData* data = [NSData dataWithContentsOfURL:url];
        
        [self generateCityBikeAnnotationsWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //inform our main controller on the main thread
            [self.delegate provider:self hasCityBikeStationsAvailable:self.cityBikeStations];
            
        });
    });
    
}

-(void)generateCityBikeAnnotationsWithData:(NSData*)data {
    
    NSError* theError;
    
    NSString* jsonString = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
    
    NSDictionary* interpretedData = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&theError];
    
    NSArray* stations = [interpretedData objectForKey:@"features"];
    
    NSMutableArray* annotations = [NSMutableArray array];
    
    //Iterate over every station and create an annotation object for it :)
    for(NSDictionary* stationInfo in stations) {
        
        CBFCityBikeStation* station = [[CBFCityBikeStation alloc] initWithJSONDictionary:stationInfo];
        [annotations addObject:station];
        
    }
    
    self.cityBikeStations = annotations;
}

@end
